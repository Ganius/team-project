document.body.onkeydown = function(i) {
    let keys = { //Клавиши
        37: 'left',
        39: 'right', //Стрелки влево и вправо
        32: 'down', //Вниз - пробелом или стрелкой вниз

    };
    if (typeof(keys[i.keyCode])!='undefined') { //Если код клавиши допустимый,
        keyPress (keys[i.keyCode]); //Передать его обработчику
        render(); //и перерисовать стакан
    }
};