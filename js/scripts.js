(function () {
    document.addEventListener('DOMContentLoaded', onReady);

    const leftMenu = document.getElementById('leftMenu');

    /**
     * @desc Инициализация приложения
     **/
    function onReady() {
        const games = [
            {id: 'zmeyka', title: 'Змейка'},
            {id: 'socer', title: 'Футбол'},
            {id: 'tetris', title: 'Тетрис'}
        ];

        buildLeftItem(games);
    }

    /**
     * @desc Построение левого меню
     * @param {Array} items
     **/
    function buildLeftItem(items) {
        if (!Array.isArray(items)) {
            return;
        }

        items.forEach(item => {
            const li = document.createElement('li');
            const a = document.createElement('a');

            a.setAttribute('href', '#');
            a.setAttribute('data-ident', item.id);
            a.onclick = openGame;
            a.innerHTML = item.title;

            li.appendChild(a);

            leftMenu.appendChild(li);
        });
    }

    /**
     *
     **/
    function openGame(event) {
        event.preventDefault();



        alert('Hhhh!!!!');
    }
})();

