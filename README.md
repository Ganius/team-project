# Тестовий проект для групової роботи

## Дошка задач
https://trello.com/b/Oo9N3SCD/team-project

## Вимоги по структурі та організації кода

1. Всі програмні модулі повинні бути організовані у IIFE функції, які повертають і встановлюють глобальну змінну.
Наприклад:

```javascript
const gameName = (function() {

  // DOM елемент, который содержит всю визуальную реализацию
  const obj = {};

  //...

  return {
    getInstance: obj
  };
})();
```


## Команди:

Паша (**TelTeam**)

| # | Прізвище | Опис |
| ----- | ----- | ----------- |
| 1 | Паша  |  |
| 2 | Паша  |  |
| 3 | Паша  |  |
| 4 | Паша  |  |


Наташа і Стас (**Bad Programmers**)

| # | Прізвище | Опис |
| ----- | ----- | ----------- |
| 1 | Наташа  | Team Lead  |
| 2 | Стас | Team Lead  | 
| 3 | Леша Коржиков | JS Developer |
| 4 | Леша Ларченко | JS Developer |
| 5 | Игорь | JS Developer |
| 6 | Сергей | JS Developer |
| 7 | Виктория | JS Developer |


Стас (**AutoBot**)

| # | Прізвище | Опис |
| ----- | ----- | ----------- |
| 1 | Стас  |  |

