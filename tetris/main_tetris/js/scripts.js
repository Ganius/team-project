let blockID;

const onKeyPress = function () {
    // document.body.onkeydown = function (event) {
    //     let keys = { //Клавиши
    //         37: 'left',
    //         39: 'right', //Стрелки влево и вправо
    //         32: 'down', //Вниз - пробелом или стрелкой вниз
    //
    //     };
    //     if (typeof (keys[event.keyCode]) != 'undefined') { //Если код клавиши допустимый,
    //         keyPress(keys[event.keyCode]); //Передать его обработчику
    //         render(); //и перерисовать стакан
    //     }
    // };

    document.addEventListener('keydown', (event) => {
        const block = document.getElementById(blockID);
        let blockLeft = Number(block.style.left.split('px')[0]);


        if (event.keyCode === 0x25) {
            if (blockLeft <= 0) {
                console.log('left < 0')
            } else {
                block.style.left = `${Number(block.style.left.split('px')[0]) - 100}px`;
            }

        } else if (event.keyCode === 0x27) {
            if (blockLeft >= 200) {
                console.log('left > 300')
            } else {
                block.style.left = `${Number(block.style.left.split('px')[0]) + 100}px`;
            }
        }
    });

};

const downBlock = function () {
    const block = document.getElementById(blockID);
    // let blockTop = Number(block.style.top.split('px')[0]);

        let timer = setInterval(function () {
            let blockTop = Number(block.style.top.split('px')[0]);
                       block.style.top = blockTop + 100 + 'px';
                       console.log(blockTop + 100 + 'px');

            if (blockTop >= 200) {
                clearInterval(timer);
                createBlock();
                downBlock();
                onKeyPress();
            }

            }, 1000);


};

const createBlock = function () {
    let newBlock = document.createElement('div');
    let canvas = document. getElementById('canvas');
    newBlock.setAttribute('class', 'newBlock');
    newBlock.setAttribute('id', `${Math.floor(Math.random()*1000)}`);
    blockID = newBlock.getAttribute('id');
    canvas.append(newBlock);


};

createBlock();
downBlock();
onKeyPress();




const globalObject = {
    cell_1: {
        cell: 0,
        row: 0,
        reserved: false
    },
    cell_2: {
        cell: 0,
        row: 100,
        reserved: false
    },
    cell_3: {
        cell: 0,
        row: 200,
        reserved: false
    },
    cell_4: {
        cell: 100,
        row: 0,
        reserved: false
    },
    cell_5: {
        cell: 100,
        row: 100,
        reserved: false
    },
    cell_6: {
        cell: 100,
        row: 200,
        reserved: false
    },
    cell_7: {
        cell: 200,
        row: 0,
        reserved: false
    },
    cell_8: {
        cell: 200,
        row: 100,
        reserved: false
    },
    cell_9: {
        cell: 200,
        row: 200,
        reserved: false
    },
    cell_10: {
        cell: 300,
        row: 0,
        reserved: false
    },
    cell_11: {
        cell: 300,
        row: 100,
        reserved: false
    },
    cell_12: {
        cell: 300,
        row: 200,
        reserved: false
    }
};
