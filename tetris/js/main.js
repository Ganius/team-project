function downBlock() {
    document.getElementById('block').onclick = function () {
        let start = Date.now();

        let timer = setInterval(function () {
            let timePassed = Date.now() - start;

            document.getElementById('block').style.top = timePassed / 6 + 'px';

            if (timePassed > 3000) clearInterval(timer);

        }, 10);
    };
}

downBlock();