const minFieldX = 1;
const maxFieldX = 500;
const minFieldY = 1;
const maxFieldY = 500;
const sideEatX = 25;
const sideEatY = 25;


function funEatX(minFieldX, maxFieldX, minSideEatX) {
    let randEatX = minFieldX + Math.random() * (maxFieldX - minFieldX + 1 - minSideEatX);
    return Math.round(randEatX);
}

function funEatY(minFieldY, maxFieldY, minSideEatY) {
    let randEatX = minFieldY + Math.random() * (maxFieldY - minFieldY + 1 - minSideEatY);
    return Math.round(randEatX);
}

let eatX = funEatX(minFieldX, maxFieldX, sideEatX);
let eatY = funEatY(minFieldY, maxFieldY, sideEatY);

console.log(eatX);
console.log(eatY);
