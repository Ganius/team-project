const minFieldX = 1;
const maxFieldX = 20;
const minFieldY = 1;
const maxFieldY = 20;
let randEatX;
let randEatY;
const food = document.createElement('div');
food.classList.add('food');
const field = document.querySelector('.field');
field.appendChild(food);

function funEatX() {
    randEatX = ((Math.random() * (maxFieldX - minFieldX)).toFixed())*25;
}
funEatX();

function funEatY() {
    randEatY = ((Math.random() * (maxFieldY - minFieldY)).toFixed())*25;
}
funEatY();

food.style.top = `${randEatY}px`;
food.style.left = `${randEatX}px`;
