function onDocumentLoaded () {
    let LeftPosition = 225;
    let TopPosition = 225;
    let snakeHead = document.getElementById('snake');

    document.onkeydown = function (event) {
        if (event.code === 'ArrowRight') {
            snakeHead.style.left = `${LeftPosition.toFixed()}px`;
            LeftPosition += 25;
        }
        else if (event.code === 'ArrowLeft') {
            snakeHead.style.left = `${LeftPosition.toFixed()}px`;
            LeftPosition -= 25;
        }
        else if (event.code === 'ArrowUp') {
            snakeHead.style.top = `${TopPosition.toFixed()}px`;
            TopPosition -= 25;
        }
        else if (event.code === 'ArrowDown') {
            snakeHead.style.top = `${TopPosition.toFixed()}px`;
            TopPosition += 25;
        }

    }
}