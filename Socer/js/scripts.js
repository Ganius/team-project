document.addEventListener("DOMContentLoaded", onDocumentReady);

const fieldWidth = 540;
const fieldHeight = 350;

const ball = document.createElement("img");
const field = document.getElementById("field-wrapper");

let leftPosition = 0;
let topPosition = 0;

const scoreWindow1 = document.querySelector(".score_team1");
const scoreWindow2 = document.querySelector(".score_team2");

let score1 = 0;
let score2 = 0;

scoreWindow1.innerText = score1;
scoreWindow2.innerText = score2;

ball.style.position = "absolute";
ball.style.width = "50px";
ball.style.height = "50px";

ball.setAttribute("src", "./img/ball.png");

document.onkeydown = function(event) {
    if (leftPosition <= 10 && topPosition >= 115 && topPosition <= 205) {
        goalScored(score1, scoreWindow1);
    }
    if (leftPosition >= 545 && topPosition >= 115 && topPosition <= 205) {
        goalScored(score2, scoreWindow2);
    }

    if (event.code === "ArrowRight") {
        if (leftPosition >= 550) return;
        leftPosition += 10;
        ball.style.left = `${leftPosition}px`;
    } else if (event.code === "ArrowLeft") {
        if (leftPosition <= 5) return;
        leftPosition -= 10;
        ball.style.left = `${leftPosition}px`;
    } else if (event.code === "ArrowUp") {
        if (topPosition <= -20) return;
        topPosition -= 10;
        ball.style.top = `${topPosition}px`;
    } else if (event.code === "ArrowDown") {
        if (topPosition >= 340) return;
        topPosition += 10;
        ball.style.top = `${topPosition}px`;
    }
};

function onDocumentReady() {
    field.appendChild(ball);
    createTeams ();

    setTimeout(function() {
        initBall();
    }, 500);
}

function startAgain() {
    initBall();
}

const button = document.getElementById("button-id");
button.onclick = function() {
    if (confirm("Are you sure?")) {
        createTeams ();
    } else return;

    startAgain();
};

/**
 * @desc Мяч по центру
 * */
function initBall() {
    leftPosition = fieldWidth / 2 + 5;
    topPosition = fieldHeight / 2 - 10;

    ball.style.top = `${topPosition.toFixed()}px`;
    ball.style.left = `${leftPosition.toFixed()}px`;
}

function goalScored(score, scoreWindow) {
    alert("Гол!!!!!");
    setTimeout(function() {
        initBall();
    }, 300);
    score++;
    scoreWindow.innerText = score;
    return false;
}

function createTeams (){
    let userTeam1 = prompt('Enter first team!');
    let userTeam2 = prompt('Enter second team!');
    if(!userTeam1) userTeam1 = 'player1';
    if(!userTeam2) userTeam2 = 'player2';

    const firstTeamInput = document.getElementsByClassName("team_name1");
    firstTeamInput[0].innerHTML = `${userTeam1}`;

    const secondTeamInput = document.getElementsByClassName("team_name2");
    secondTeamInput[0].innerHTML = `${userTeam2}`;

}
